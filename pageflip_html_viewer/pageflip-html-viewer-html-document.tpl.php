<?php

/**
 * @file
 * HTML page template for PageFlip HTML/JavaScript viewer.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <?php print $head; ?>
    <title><?php print $title; ?></title>
    <?php print $styles; ?>
    <?php print $scripts; ?>
   </head>
  <body>

    <?php print $content; ?>

  </body>
</html>
